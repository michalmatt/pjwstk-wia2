#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

#define ROZPOCZNIJ_PROGRAM 1
#define ZAKONCZ_PROGRAM 2

const string nazwaProgramu = "CONVERTER";

void piszWynik(int input) {
	cout << "\nLiczba " << input << " (w systemie dziesiatkowym) zamieniona na system osemkowy (oktalny) ma postac (16-bitow):" << endl;
}

int main() {
	while (true) {
		cout << "Witamy w programie " << nazwaProgramu << " zamieniajacym liczbe w systemie dziesiatkowym na liczbe w systemie osemkowym." << endl;
		cout << "Wpisz numer pozycji z MENU ponizej i nacisnij ENTER.\n(1) Rozpocznij program\n(2) Zakoncz program" << endl;
		int wybor;
		cin >> wybor;
		switch (wybor) {
		case ROZPOCZNIJ_PROGRAM:
		{
			int podanaLiczba;
			cout << "Podaj liczbe w systemie dziesiatkowym, ktora chcesz zamienic (ograniczenie dla liczb ujemnych: -32768)." << endl;
			cout << "Liczba do zamiany = "; cin >> podanaLiczba;

			if (podanaLiczba >= 0) {
				int biezacaLiczba;
				int tabOsemkowa[100];
				int i;

				biezacaLiczba = podanaLiczba;
				for (i = 0; biezacaLiczba >= 8; i++) {
					tabOsemkowa[i] = biezacaLiczba % 8;
					biezacaLiczba /= 8;
				}
				tabOsemkowa[i] = biezacaLiczba;
				piszWynik(podanaLiczba);
				while (i >= 0) {
					cout << tabOsemkowa[i];
					i--;
				}
			}
			else if (podanaLiczba < 0) {
				int biezacaLiczba;
				int tabDwojkowa[100];
				int i;

				biezacaLiczba = pow(2, 16) + podanaLiczba;
				for (i = 0; biezacaLiczba >= 2; i++) {
					tabDwojkowa[i] = biezacaLiczba % 2;
					biezacaLiczba /= 2;
				}
				tabDwojkowa[i] = biezacaLiczba;
				int numerMaxIndeksu = i;
				cout << "\nLiczba " << podanaLiczba << " (w systemie dziesiatkowym) zamieniona na system dwojkowy (binarny) ma postac (16-bitow):" << endl;
				while (i >= 0) {
					cout << tabDwojkowa[i];
					i--;
				}
				string tabOsemkowa[100];
				int numerMaxIndeksu3bit = numerMaxIndeksu / 3;

				tabOsemkowa[0] = to_string(0) + to_string(0) + to_string(tabDwojkowa[numerMaxIndeksu]);
				int j;
				for (i = 1, j = 1; i <= numerMaxIndeksu3bit; i++, j += 3) {
					tabOsemkowa[i] = to_string(tabDwojkowa[numerMaxIndeksu - j]) + to_string(tabDwojkowa[numerMaxIndeksu - (j + 1)]) + to_string(tabDwojkowa[numerMaxIndeksu - (j + 2)]);
				}
				piszWynik(podanaLiczba);
				for (i = 0; i <= numerMaxIndeksu3bit; i++) {
					if (tabOsemkowa[i] == "000") {
						cout << "0";
					}
					else if (tabOsemkowa[i] == "001") {
						cout << "1";
					}
					else if (tabOsemkowa[i] == "010") {
						cout << "2";
					}
					else if (tabOsemkowa[i] == "011") {
						cout << "3";
					}
					else if (tabOsemkowa[i] == "100") {
						cout << "4";
					}
					else if (tabOsemkowa[i] == "101") {
						cout << "5";
					}
					else if (tabOsemkowa[i] == "110") {
						cout << "6";
					}
					else if (tabOsemkowa[i] == "111") {
						cout << "7";
					}
				}

			}

			_getch();
			system("cls");
		}
		continue;

		case ZAKONCZ_PROGRAM:
		{
			cout << "Nacisnij ENTER, aby potwierdzic wyjscie z programu " << nazwaProgramu << "...";
			_getch();
			return EXIT_SUCCESS;
		}
		break;

		default:
		{
			cout << "Nie ma takiej pozycji. Nacisnij ENTER, aby wybrac ponownie.";
			_getch();
			system("cls");
			continue;
		}

		}
	}
}